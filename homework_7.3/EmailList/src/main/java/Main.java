import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static final String WRONG_EMAIL_ANSWER = "Неверный формат email";
    public static EmailList emailList = new EmailList();
    public static String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    
    /* TODO:
        Пример вывода списка Email, после ввода команды LIST в консоль:
        test@test.com
        hello@mail.ru
        - каждый адрес с новой строки
        - список должен быть отсортирован по алфавиту
        - email в разных регистрах считается одинаковыми
           hello@skillbox.ru == HeLLO@SKILLbox.RU
        - вывод на печать должен быть в нижнем регистре
           hello@skillbox.ru
        Пример вывода сообщения об ошибке при неверном формате Email:
        "Неверный формат email"
    */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String command = scanner.nextLine();
            if (command.equals("0")) {
                break;
            }

            //TODO: write code here
            if (command.contains("ADD")) {
                emailList.add(command.replaceAll("ADD ", ""));
            } else if (command.matches("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$")) {
                emailList.add(command.toLowerCase());
            } else if (command.contains("LIST")) {
                for (int i = 0; i < emailList.getSortedEmails().size(); i++) {
                    System.out.println(i + " - " + emailList.getSortedEmails().get(i));
                }
            } else {
                System.out.println(WRONG_EMAIL_ANSWER);
            }
        }
    }
}