import java.util.*;

public class EmailList {
    TreeSet<String> emailList = new TreeSet<>();
    public void add(String email) {
        // TODO: валидный формат email добавляется
       email.toLowerCase();
       if((email.toLowerCase().matches(".+@.+\\..+")) && (!(emailList.contains(email)))){
           emailList.add(email.toLowerCase());
       }
    }

    public List<String> getSortedEmails() {
        // TODO: возвращается список электронных адресов в алфавитном порядке
        ArrayList<String> list = new ArrayList<>();
        list.addAll(emailList);
        for(int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }

        return list;
    }

}
