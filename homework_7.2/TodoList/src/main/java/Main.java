import java.util.Scanner;

public class Main {
        private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        // TODO: написать консольное приложение для работы со списком дел todoList
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String command = scanner.nextLine();
            if (command.equals("0")) {
                break;
            }

            String[] commandWords = command.split(" ");
            boolean isNumber = true;
            for (int i = 0; i < commandWords[1].length(); i++) {
                if(!Character.isDigit(commandWords[1].charAt(i)))
                    isNumber = false;
            }

            switch (commandWords[0]) {
                case ("ADD"): {
                    todoList.add(command.replaceAll("ADD ", ""));
                }
                break;
                case ("LIST"): {
                    for (int i = 0; i < todoList.getTodos().size(); i++) {
                        System.out.println(i + " - " + todoList.getTodos().get(i));
                    }
                }
                break;
                case ("DELETE"): {
                    if (isNumber) todoList.delete(Integer.parseInt(commandWords[1]));
                    else System.out.println("Индекс " + commandWords[1] + " не является числом");
                }
                break;
                case ("EDIT"): {
                    if (isNumber) {
                        todoList.edit(
                                command.replaceAll(commandWords[0] + " " + commandWords[1] + " ", "")
                                , Integer.parseInt(commandWords[1]));
                    }
                    else System.out.println("Индекс " + commandWords[1] + " не является числом");
                }
                break;

            }
        }
    }
}