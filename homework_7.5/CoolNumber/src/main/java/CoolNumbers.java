import java.util.*;

public class CoolNumbers extends Main {
    public static List<String> generateCoolNumbers() {
        String[] lettersNumber = {"A","B","E","K","M","H","O","P","C","T","Y","X"};
        int[] carNumbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] regionNumbers = new int[199];
        for(int i = 1; i < regionNumbers.length; i++){
            regionNumbers[i] = (i + 1);
        }


        for(int i = 1; i < 200; i++ ){

        }
        List<String>list = new ArrayList<>();
        String number = "";

        for(int i = 0; i < 2000000; i++){

                    Random r = new Random();
                    int randomInt1 = r.nextInt(carNumbers.length);
                    int randomInt2 = r.nextInt(regionNumbers.length);
                    int randomIndex1 = r.nextInt(lettersNumber.length);
                    int randomIndex2 = r.nextInt(lettersNumber.length);
                    int randomIndex3 = r.nextInt(lettersNumber.length);
                    String letter1 = lettersNumber[randomIndex1];
                    String letter2 = lettersNumber[randomIndex2];
                    String letter3 = lettersNumber[randomIndex3];
                    String region = String.valueOf(randomInt2);
                    if(randomInt2 == 0){
                        randomInt2 = 1;
                    }



                         else if (randomInt2 < 10 && randomInt2 != 0) {
                            region = "0" + region;
                            number = String.format("%s%d%d%d%s%s%s", letter1, randomInt1, randomInt1, randomInt1, letter2, letter3, region);
                        } else {

                            number = String.format("%s%d%d%d%s%s%s", letter1, randomInt1, randomInt1, randomInt1, letter2, letter3, region);
                        }



            list.add(number);
        }
        return list;
    }

    public static boolean bruteForceSearchInList(List<String> list, String number) {
        if(list.contains(number)){
            return true;
        }else
        return false;
    }

    public static boolean binarySearchInList(List<String> sortedList, String number) {
        if(sortedList.contains(number)){
            return true;
        } else
        return false;
    }


    public static boolean searchInHashSet(HashSet<String> hashSet, String number) {
        if(hashSet.contains(number)){
            return true;
        } else
        return false;
    }

    public static boolean searchInTreeSet(TreeSet<String> treeSet, String number) {
        if(treeSet.contains(number)){
            return  true;
        } else
        return false;
    }

}
