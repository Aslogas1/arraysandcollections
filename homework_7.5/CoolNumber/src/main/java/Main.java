import java.util.*;

public class Main {
    private static int repeatRecursion = 1;
    private static int countRecursion = 0;

    public static void main(String[] args) {
        char[] letters = {'А', 'В', 'Е', 'К', 'М', 'Н', 'О', 'Р', 'С', 'Т', 'У', 'Х'};
        TreeSet<String> setLetters = new TreeSet<>();
        System.out.println("Создание ArrayList...");
        makeSetOfLetters(letters, "", setLetters);
        ArrayList<String> carNumbers = new ArrayList<>();

        for (String set : setLetters) {
            for (int i = 1; i < 10; i++) {
                for (int j = 1; j <= 199; j++) {
                    String region = (j < 10) ? "0" + j : String.valueOf(j);
                    String numberCar = String.format("%s%d%d%d%s%s", set.charAt(0), i, i, i, set.substring(1), region);
                    carNumbers.add(numberCar);
                }
            }
        }
        System.out.println("Создан ArrayList. Количество записей: " + carNumbers.size());

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Введите номер машины для поиска:");
            String input = scanner.nextLine();
            findBySortArrayList(input, carNumbers);
            findByBinarySearch(input, carNumbers);
            findInHashSet(input, carNumbers);
            findInTreeSet(input, carNumbers);
            if (input.equals("0")) {
                break;
            }
        }
    }

    private static void findBySortArrayList(String number, ArrayList<String> numbers) {
        boolean isContains = false;
        long start = System.nanoTime();
        for (String elem : numbers) {
            if (elem.contains(number)) {
                isContains = true;
                break;
            }
        }
        if (isContains) {
            System.out.println("Поиск перебором: номер найден, поиск занял " + (System.nanoTime() - start) + "нс");
        } else {
            System.out.println("Поиск перебором: номер не найден, поиск занял " + (System.nanoTime() - start) + "нс");
        }
    }

    private static void findByBinarySearch(String number, ArrayList<String> numbers) {
        long start = System.nanoTime();
        if (Collections.binarySearch(numbers, number) >= 0) {
            System.out.println("Бинарный поиск: номер найден, поиск занял " + (System.nanoTime() - start) + "нс");
        } else {
            System.out.println("Бинарный поиск: номер не найден, поиск занял " + (System.nanoTime() - start) + "нс");
        }
    }

    private static void findInHashSet(String number, ArrayList<String> numbers) {
        HashSet<String> list = new HashSet<>(numbers);
        long start = System.nanoTime();
        if (list.contains(number)) {
            System.out.println("Поиск в HashSet: номер найден, поиск занял " + (System.nanoTime() - start) + "нс");
        } else {
            System.out.println("Поиск в HashSet: номер не найден, поиск занял " + (System.nanoTime() - start) + "нс");
        }

    }

    private static void findInTreeSet(String number, ArrayList<String> numbers) {
        TreeSet<String> list = new TreeSet<>(numbers);
        long start = System.nanoTime();
        if (list.contains(number)) {
            System.out.println("Поиск в TreeSet: номер найден, поиск занял " + (System.nanoTime() - start) + "нс");
        } else {
            System.out.println("Поиск в TreeSet: номер не найден, поиск занял " + (System.nanoTime() - start) + "нс");
        }
    }

    //рекурсия для формирования всех возможных наборов трёх букв из 12 символов массива
    private static void makeSetOfLetters(char[] letters, String set, TreeSet<String> makeSetLetters) {
        String memorySet = set;
        for (char elem : letters) {
            set = memorySet + elem;
            if (set.length() == 3) {
                makeSetLetters.add(set);
            }
            countRecursion++;
            if (repeatRecursion != 3) {
                repeatRecursion++;
                countRecursion--;
                makeSetOfLetters(letters, set, makeSetLetters);
            }
        }
        if (countRecursion % (letters.length * letters.length) == 0) {
            repeatRecursion = 1;
        } else {
            repeatRecursion = 2;
        }
    }
}
