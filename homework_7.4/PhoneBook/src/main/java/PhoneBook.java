import org.antlr.v4.runtime.tree.Tree;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneBook {

    private TreeMap<String, String> phoneMap = new TreeMap<>();

   

    public boolean isPhoneCorrect(String name) {

        Pattern phonePattern = Pattern.compile("^\\+?[7-8][0-9]{10}");
        Matcher matcher = phonePattern.matcher(name);

        return matcher.matches();
    }

    public boolean isNameCorrect(String phone) {

        Pattern namePattern = Pattern.compile("\\D*");
        Matcher matcher = namePattern.matcher((phone));

        return matcher.matches();
    }

    public void addContact(String phone, String name) {

        if (isNameCorrect(name) && isPhoneCorrect(phone)) {
            phoneMap.put(phone, name);
        }
    }


    public String getNameByPhone(String phone) {
        StringBuilder nameWithPhones = new StringBuilder();

        phoneMap.entrySet()
                .stream()
                .filter(pair -> pair.getKey().equals(phone))
                .forEach(pair -> {
                    if (nameWithPhones.length() == 0) {
                        nameWithPhones.append(pair.getValue()).append(" - ");
                    }
                    nameWithPhones.append(pair.getKey()).append(" ");
                });

        return nameWithPhones.toString().trim();
    }

    public Set<String> getPhonesByName(String name) {

        StringBuilder nameWithPhones = new StringBuilder();
        TreeSet<String> phoneSet = new TreeSet<>();

        phoneMap.entrySet()
                .stream()
                .filter(pair -> pair.getValue().equals(name))
                .forEach(pair -> {
                    if (nameWithPhones.length() == 0) {
                        nameWithPhones.append(name).append(" - ");
                    }
                    nameWithPhones.append(pair.getKey()).append(" ");
                });

        if (nameWithPhones.length() > 0) {
            phoneSet.add(nameWithPhones.toString().trim());
        }

        return phoneSet;
    }

    public Set<String> getAllContacts() {

        TreeSet<String> phoneSet = new TreeSet<>();

        phoneMap.forEach((key, value) -> {
            StringBuilder builder = new StringBuilder();
            builder.append(value).append(" - ");
            phoneMap.entrySet()
                    .stream()
                    .filter(pair -> pair.getValue().equals(value))
                    .forEach(pair -> builder.append(pair.getKey()).append(" "));
            phoneSet.add(builder.toString().trim());
        });

        return phoneSet;
    }
    public void  getAllPhones(){

    }
}