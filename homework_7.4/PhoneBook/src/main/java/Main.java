import java.util.Scanner;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        TreeMap<String, String> phoneBook = new TreeMap<>();
        TreeMap<String, String> phoneBookBackwards = new TreeMap<>();
        System.out.println("Введите номер, имя или команду:");
        Scanner scanner = new Scanner(System.in);
        for (; ; ) {
            String input = scanner.nextLine().trim();


            if (phoneBook.containsKey(input)) {
                System.out.println(input + " - " + phoneBook.get(input));
                continue;
            }
            if (phoneBook.containsValue(input)) {
                System.out.println(phoneBookBackwards.get(input) + " - " + input);
                continue;
            }
            if(input.equals("LIST")){
                System.out.println(phoneBook.entrySet());
            }


            if (phoneCheck(input)) {
                System.out.println("Введите имя абонента: ");
                String input2 = scanner.nextLine().trim();
                for (; ; ) {
                    if (nameCheck(input2)) {
                        phoneBook.put(input2, input);
                        System.out.println("Контакт сохранен!");
                        phoneBookBackwards.put(input, input2);
                        break;
                    } else {
                        System.out.println("Недопустимое имя");
                        input2 = scanner.nextLine();
                    }
                }
                continue;
            }


            if (nameCheck(input)) {
                System.out.println("Введите номер абонента: ");
                String input2 = scanner.nextLine().trim();
                for (; ; ) {
                    if (phoneCheck(input2)) {
                        phoneBook.put(input, input2);
                        System.out.println("Контакт сохранен!");
                        phoneBookBackwards.put(input2, input);
                        break;
                    }
                     else {
                        System.out.println("Недопустимый номер");
                        input2 = scanner.nextLine();
                    }
                }
                continue;
            }
        }
    }

    public static Boolean phoneCheck(String input) {
        return input.matches("^\\+?[7-8][0-9]{10}");

    }

    public static Boolean nameCheck(String input) {
        return input.matches("[а-яА-Яa-zA-Z-\\s]*[^(\\s\\s)][^\\s][^-]$");
    }
}
