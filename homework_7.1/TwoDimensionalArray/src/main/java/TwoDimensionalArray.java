public class TwoDimensionalArray {
    public static char symbol = 'X';

    public static char[][] getTwoDimensionalArray(int size) {
        char[][] arr = new char[size][size];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (i == j || i + j == (size - 1)) {
                    arr[i][j] = symbol;
                } else {
                    arr[i][j] = ' ';
                }
            }


            //TODO: Написать метод, который создаст двумерный массив char заданного размера.
            // массив должен содержать символ symbol по диагоналям, пример для size = 3
            // [X,  , X]
            // [ , X,  ]
            // [X,  , X]
        }
            return arr;

    }
}
