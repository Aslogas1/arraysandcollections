public class ReverseArray {

    //TODO: Напишите код, который меняет порядок расположения элементов внутри массива на обратный.
    public static String[] reverse (String[] strings) {
       String buffer;
        for (int i = 0, j = strings.length - 1; i <  strings.length / 2; i++, j--) {
            buffer = strings[j];
            strings[j] = strings[i];
            strings[i] = buffer;
        }
            return strings;
        }
}
