import java.text.DecimalFormat;
import java.util.Arrays;

public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {
        float[] temperatureData = new float[patientsCount];
        float min = 32.0f;
        float max = 40.0f;
        float range = max - min;
        for(int i = 0; i < temperatureData.length; i++){
            temperatureData[i] = (float)(Math.random() * range) + min;
        }

        //TODO: напишите метод генерации массива температур пациентов

        return temperatureData;
    }

    public static String getReport(float[] temperatureData) {
        float average;
        float sum = 0.0f;
        int heath = 0;
        for(int i = 0; i < temperatureData.length; i++){
            sum +=  temperatureData[i];
            if(temperatureData[i] >= 36.2f && temperatureData[i] < 37.0f){
               heath++;
            }
        }
        String total = Arrays.toString(temperatureData);
        total = total.replaceAll(",","").replaceAll("\\[","").replaceAll("\\]","");
        System.out.println(total);
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */
        average = sum / temperatureData.length;
        DecimalFormat format = new DecimalFormat("#.##");
        String result = format.format(average);


        String report =
                "Температуры пациентов: " + total +
                        "\nСредняя температура: " + result +
                        "\nКоличество здоровых: " + heath;

        return report;
    }
}
